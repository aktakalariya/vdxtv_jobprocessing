package com.vdxtv.jobprocessing;

import java.util.Date;
import java.util.Timer;
import java.util.TimerTask;

import org.apache.log4j.Logger;

public class JobProcessingSystemImpl implements JobProcessingSystem {

	static final Logger logger = Logger.getLogger(JobProcessingSystemImpl.class);
	static {
		AuditStatistics.startAudit();
	}

	@Override
	public void run(final Job job, long futureExecutionTimeInMillis) {
		logger.info("Scheduling job=" + job.getId());

		Timer timer = new Timer();
		TimerTask timerTask = new TimerTask() {
			@Override
			public void run() {
				long start = System.currentTimeMillis();
				logger.info("Starting job = " + job.getId());
				try {
					AuditStatistics.TOTAL_JOBS_PROCESSED++;
					job.run();
					AuditStatistics.SUCCESS_JOBS++;
					AuditStatistics.TOTAL_TIME += (System.currentTimeMillis() - start);
					logger.info("Ending job = " + job.getId() + ", Total time(ms)=" + (System.currentTimeMillis() - start) + ", Success");
				} catch (Exception ex) {
					logger.info("Ending job = " + job.getId() + ", Total time(ms)=" + (System.currentTimeMillis() - start) + ", Failed");
					AuditStatistics.FAILED_JOBS++;
					AuditStatistics.TOTAL_TIME += (System.currentTimeMillis() - start);
				}
			}
		};
		timer.schedule(timerTask, new Date(futureExecutionTimeInMillis));
		AuditStatistics.TOTAL_JOBS_RECEIVED++;
	}

}
