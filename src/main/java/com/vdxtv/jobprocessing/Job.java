package com.vdxtv.jobprocessing;

public interface Job {
	void run();
	String getId();
}
