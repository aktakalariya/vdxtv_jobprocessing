package com.vdxtv.jobprocessing;

import java.util.UUID;

import junit.framework.TestCase;

/**
 * Unit test for  App.
 */
public class AppTest extends TestCase {

	public void testApp() {
		JobProcessingSystemImpl jobProcessingSystemImpl = new JobProcessingSystemImpl();
		long futureTime=1627232220000l;//Modify this to test
		Job job1 = new Job() {

			@Override
			public void run() {
				StringBuilder sb = new StringBuilder();
				for (int i = 0; i < 1000; i++) {
					sb.append(i);
				}
			}

			@Override
			public String getId() {
				return UUID.randomUUID().toString();
			}
		};
		jobProcessingSystemImpl.run(job1, futureTime);
		try {
			Thread.sleep(400000);
		} catch (InterruptedException e) {
			// TODO Auto-generated catch block
			e.printStackTrace();
		}
	}
}
