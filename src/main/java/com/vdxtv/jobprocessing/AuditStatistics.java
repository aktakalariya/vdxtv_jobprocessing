package com.vdxtv.jobprocessing;

import java.sql.Time;
import java.util.Timer;
import java.util.TimerTask;

import org.apache.log4j.Logger;

public class AuditStatistics {

	static final Logger logger = Logger.getLogger(AuditStatistics.class);

	public static int TOTAL_JOBS_RECEIVED = 0;
	public static int TOTAL_JOBS_PROCESSED = 0;
	public static long TOTAL_TIME = 0;
	public static int FAILED_JOBS = 0;
	public static int SUCCESS_JOBS = 0;
	private static int FAIL_RATE = 0;
	private static int SUCCESS_RATE = 0;
	private static double AVG_TIME = 0.0;
	
	private static long MONITORING_INTERVAL=30000;

	public static void startAudit() {
		logger.info("Statistics Audit started................");
		new Timer().schedule(new TimerTask() {

			@Override
			public void run() {
				if (TOTAL_JOBS_PROCESSED > 0) {
					FAIL_RATE = FAILED_JOBS * 100 / TOTAL_JOBS_PROCESSED;
					SUCCESS_RATE = SUCCESS_JOBS * 100 / TOTAL_JOBS_PROCESSED;
					AVG_TIME = TOTAL_TIME / TOTAL_JOBS_PROCESSED;
					logger.info("{\"total_jobs\":" + TOTAL_JOBS_PROCESSED + ",\"success_rate(%)\":" + SUCCESS_RATE + ",\"failure_rate(%)\":" + FAIL_RATE + ",\"avg_time\":" + AVG_TIME + "}");
				}
			}
		}, 0, MONITORING_INTERVAL);
	}

}
