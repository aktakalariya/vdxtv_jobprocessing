package com.vdxtv.jobprocessing;

public interface JobProcessingSystem {
	public void run(Job job, long futureExecutionTimeInMillis);
}
